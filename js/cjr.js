"use strict"

var makeRequest = function() {

	$('#loadGif').css('opacity','1');

	var originalURL = 'http://api.icndb.com/jokes/';
	var countURL = $('#jokesNumber').val();
	var nameURL = $('#jokesName').val();
	var surnameURL = $('#jokesSurname').val();
	var URL = originalURL + "random/" + countURL + "?firstName=" + nameURL + "&lastName=" + surnameURL;

	var XHR = ("onload" in new XMLHttpRequest()) ? XMLHttpRequest : XDomainRequest;

	var jokesXHR = new XHR;
	var jokes;
	jokesXHR.open('GET', URL, true);
	jokesXHR.onload = function() {
		console.log(this.status);
		if(this.status == 200){
 			jokes = JSON.parse(jokesXHR.responseText);
 			var source = $('#joke-template').html(); 
			var template = Handlebars.compile(source);
			$('#jokeplace').empty();
			$('#jokeplace').append(template(jokes));
			$('#loadGif').css('opacity','0');
			$('#errorstatus').css('display','none');
			console.log("Jokes were successfully loaded");
		}
		else{
			var errorMSG;
			if(this.status == 500){
       			errorMSG = "500 Internal Server Error";
      		}
      		if(this.status == 422){
      			errorMSG = "422 Unprocessable Entity";
      		}
      		if(this.status == 405){
        		errorMSG = "405 Method Not Allowed";
      		}
      		if(this.status == 404){
        		errorMSG = "404 Not Found";
      		}
      		if(this.status == 403){
        		errorMSG = "403 Forbidden";
      		}
      		if(this.status == 401){
        		errorMSG = "401 Unauthorized";
      		}
      		if(this.status == 204){
        		errorMSG = "204 No Content";
      		}
      		if(this.status == 201){
        		errorMSG = "201 Continue";
      		}
      		console.log(errorMSG);
      		$('#errorstatus').css('display','block');
	 		$('#errorstatus').html(errorMSG);
		}
	}
	jokesXHR.onerror = function() {
	 	console.log("Error " + this.status);
	 	if(this.status == 0) {
	 		$('#errorstatus').css('display','block');
	 		$('#errorstatus').html('Error 0: no connection');
	 	}
	}
	jokesXHR.send();
}

$(document).ready(function () {
	makeRequest();
	setInterval(makeRequest, 7000);
	$('#reqbutton').on("click", function() {
		makeRequest();
	});
});